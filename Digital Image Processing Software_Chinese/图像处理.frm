VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form 图像处理 
   Caption         =   "图像"
   ClientHeight    =   10950
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   16080
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   10950
   ScaleWidth      =   16080
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text2 
      Height          =   500
      Left            =   14520
      TabIndex        =   31
      Top             =   4320
      Width           =   500
   End
   Begin VB.TextBox Text1 
      Height          =   500
      Left            =   13680
      TabIndex        =   30
      Top             =   4320
      Width           =   500
   End
   Begin VB.CommandButton 最大直径提取法二 
      Caption         =   "最大直径提取法二"
      Height          =   500
      Left            =   12000
      TabIndex        =   29
      Top             =   2400
      Width           =   1500
   End
   Begin VB.CommandButton 质心提取 
      Caption         =   "质心提取"
      Height          =   500
      Left            =   10320
      TabIndex        =   28
      Top             =   2400
      Width           =   1500
   End
   Begin VB.CommandButton 最大直径提取法一 
      Caption         =   "最大直径提取（法1）"
      Height          =   500
      Left            =   8640
      TabIndex        =   27
      Top             =   2400
      Width           =   1500
   End
   Begin VB.PictureBox win1 
      AutoRedraw      =   -1  'True
      Height          =   6000
      Left            =   360
      ScaleHeight     =   5940
      ScaleWidth      =   5940
      TabIndex        =   25
      Top             =   120
      Width           =   6000
      Begin VB.PictureBox pic1 
         Height          =   735
         Left            =   3120
         ScaleHeight     =   675
         ScaleWidth      =   1155
         TabIndex        =   26
         Top             =   2160
         Visible         =   0   'False
         Width           =   1215
      End
      Begin MSComDlg.CommonDialog dlg1 
         Left            =   1320
         Top             =   1080
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
   End
   Begin VB.CommandButton 轮廓提取 
      Caption         =   "轮廓提取"
      Height          =   500
      Left            =   6960
      TabIndex        =   24
      Top             =   2400
      Width           =   1500
   End
   Begin VB.CommandButton 图像细化 
      Caption         =   "图像细化"
      Height          =   500
      Left            =   12000
      TabIndex        =   23
      Top             =   1680
      Width           =   1500
   End
   Begin VB.CommandButton 二值化 
      Caption         =   "二值化"
      Height          =   500
      Left            =   10320
      TabIndex        =   22
      Top             =   1680
      Width           =   1500
   End
   Begin VB.CommandButton 扩大 
      Caption         =   "扩大"
      Height          =   500
      Left            =   8640
      TabIndex        =   21
      Top             =   1680
      Width           =   1500
   End
   Begin VB.CommandButton 收缩 
      Caption         =   "收缩"
      Height          =   500
      Left            =   6960
      TabIndex        =   20
      Top             =   1680
      Width           =   1500
   End
   Begin VB.CommandButton 中值滤波 
      Caption         =   "中值滤波"
      Height          =   495
      Left            =   4200
      TabIndex        =   19
      Top             =   7920
      Width           =   1695
   End
   Begin VB.TextBox 卷积值 
      Height          =   375
      Index           =   8
      Left            =   2880
      TabIndex        =   18
      Top             =   8280
      Width           =   735
   End
   Begin VB.TextBox 卷积值 
      Height          =   375
      Index           =   7
      Left            =   1800
      TabIndex        =   17
      Top             =   8280
      Width           =   735
   End
   Begin VB.TextBox 卷积值 
      Height          =   375
      Index           =   6
      Left            =   720
      TabIndex        =   16
      Top             =   8280
      Width           =   735
   End
   Begin VB.TextBox 卷积值 
      Height          =   375
      Index           =   5
      Left            =   2880
      TabIndex        =   15
      Top             =   7680
      Width           =   735
   End
   Begin VB.TextBox 卷积值 
      Height          =   375
      Index           =   4
      Left            =   1800
      TabIndex        =   14
      Top             =   7680
      Width           =   735
   End
   Begin VB.TextBox 卷积值 
      Height          =   375
      Index           =   3
      Left            =   720
      TabIndex        =   13
      Top             =   7680
      Width           =   735
   End
   Begin VB.TextBox 卷积值 
      Height          =   375
      Index           =   2
      Left            =   2880
      TabIndex        =   12
      Top             =   7200
      Width           =   735
   End
   Begin VB.TextBox 卷积值 
      Height          =   375
      Index           =   1
      Left            =   1800
      TabIndex        =   11
      Top             =   7200
      Width           =   735
   End
   Begin VB.TextBox 卷积值 
      Height          =   375
      Index           =   0
      Left            =   720
      TabIndex        =   10
      Top             =   7200
      Width           =   735
   End
   Begin VB.CommandButton 卷积 
      Caption         =   "卷积"
      Height          =   495
      Left            =   4200
      TabIndex        =   9
      Top             =   7200
      Width           =   1695
   End
   Begin VB.CommandButton 均衡直方图 
      Caption         =   "均衡直方图"
      Height          =   500
      Left            =   10320
      TabIndex        =   8
      Top             =   960
      Width           =   1500
   End
   Begin VB.CommandButton 直方图 
      Caption         =   "直方图"
      Height          =   500
      Left            =   12000
      TabIndex        =   7
      Top             =   960
      Width           =   1500
   End
   Begin VB.CommandButton Command1 
      Caption         =   "灰度变化"
      Height          =   500
      Left            =   8640
      TabIndex        =   6
      Top             =   960
      Width           =   1500
   End
   Begin VB.CommandButton change 
      Caption         =   "反色"
      Height          =   500
      Left            =   12000
      TabIndex        =   5
      Top             =   240
      Width           =   1500
   End
   Begin VB.CommandButton colortogray 
      Caption         =   "提取灰度图像"
      Height          =   500
      Left            =   6960
      TabIndex        =   4
      Top             =   960
      Width           =   1500
   End
   Begin VB.CommandButton save 
      Caption         =   "保存"
      Height          =   500
      Left            =   10320
      TabIndex        =   3
      Top             =   240
      Width           =   1500
   End
   Begin VB.CommandButton end 
      Caption         =   "结束"
      Height          =   500
      Left            =   8640
      TabIndex        =   2
      Top             =   240
      Width           =   1500
   End
   Begin VB.CommandButton read 
      Caption         =   "输入图片"
      Height          =   500
      Left            =   6960
      TabIndex        =   1
      Top             =   240
      Width           =   1500
   End
   Begin VB.PictureBox win2 
      AutoRedraw      =   -1  'True
      Height          =   6000
      Left            =   6960
      ScaleHeight     =   5940
      ScaleWidth      =   5940
      TabIndex        =   0
      Top             =   3000
      Width           =   6000
   End
   Begin VB.Label Label1 
      Caption         =   "质心坐标（xc,yc）"
      Height          =   300
      Left            =   13680
      TabIndex        =   32
      Top             =   3960
      Width           =   1500
   End
End
Attribute VB_Name = "图像处理"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim a1, a2, e, s, d, i, j, k, m, n As Integer
Dim c, f, h, l, o, t, r, g, b, X, th, R1, R2, u1, u2, w() As Long
Dim n1() As Double
Dim p(), q(), qq(), hd() As Double
Dim pic(), RZ() As Double


Private Sub change_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
r = 0: g = 0: b = 0
For i = 0 To win1.ScaleWidth
 For j = 0 To win1.ScaleHeight
 c = win1.Point(i, j)
 r = 255 - (c And &HFF)
 g = 255 - ((c And 65280) \ 256)
 b = 255 - ((c And &HFF0000) \ 65536)
  win2.PSet (i, j), RGB(r, g, b)
 Next j
 Next i
End Sub

Private Sub colortogray_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
r = 0: g = 0: b = 0
For i = 0 To win1.ScaleWidth
 For j = 0 To win1.ScaleHeight
 
 c = win1.Point(i, j)
 r = c And &HFF
 g = (c And 65280) \ 256
 b = (c And &HFF0000) \ 65536
 
 X = (r + g + b) / 3
 If X > 255 Then X = 255
 If X < 0 Then X = 0
 win2.PSet (i, j), RGB(X, X, X)
 Next j
 Next i

End Sub

Private Sub Command1_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
r = 0: g = 0: b = 0
For i = 0 To win1.ScaleWidth
 For j = 0 To win1.ScaleHeight
  c = win1.Point(i, j)
 r = c And &HFF
 X = (Sqr(r) / 16) * 255
 If X > 255 Then X = 255
 If X < 0 Then X = 0
 win2.PSet (i, j), RGB(X, X, X)
  Next j
 Next i
End Sub



Private Sub Command2_Click()

End Sub

Private Sub end_Click()
End 'exit
End Sub

Private Sub read_Click()
win1.Cls
win1.ScaleMode = 1
dlg1.DialogTitle = "打开图像"
dlg1.Filter = "pictures(*.bmp)|*.*"
dlg1.ShowOpen

dlg1.CancelError = True
On Error GoTo errhandler

pic1.Picture = LoadPicture(dlg1.FileName)

DoEvents
win1.PaintPicture pic1.Picture, 0, 0, win1.Width, win1.Height

Exit Sub
errhandler:
Exit Sub

End Sub

Private Sub save_Click()
win2.AutoRedraw = True
win2.AutoSize = True
dlg1.ShowSave
SavePicture win2.Image, dlg1.FileName

End Sub

Private Sub 二值化_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
n = win1.ScaleWidth
m = win1.ScaleHeight
For i = 0 To n
For j = 0 To m
c = win2.Point(i, j)
r = c And &HFF
s = s + r
Next j
Next i
th = s / (n * m)
t = 0
Do While Abs(th - t) >= 1
th = t
For i = 0 To n
For j = 0 To m
c = win2.Point(i, j)
r = c And &HFF
If r > th Then
a1 = a1 + 1
R1 = R1 + r
End If
If r <= th Then
a2 = a2 + 1
R2 = R2 + r
End If
Next j
Next i
If a1 = 0 Then
u1 = 0
Else: u1 = R1 / a1
End If
If a2 = 0 Then
u2 = 0
Else: u2 = R2 / a2
End If
t = (u1 + u2) / 2
Loop


For i = 0 To n
For j = 0 To m
c = win2.Point(i, j)
r = c And &HFF
If r > t Then r = 255
If r <= t Then r = 0
win1.PSet (i, j), RGB(r, r, r)
Next j
Next i

End Sub

Private Sub 卷积_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
ReDim w(8) As Long
For k = 0 To 8
w(k) = Val(卷积值(k).Text)
Next k


n = win1.ScaleWidth
m = win1.ScaleHeight
ReDim pic(2, n, m) As Long
For i = 0 To n
For j = 0 To m
c = win1.Point(i, j)
pic(0, i, j) = c And &HFF
pic(1, i, j) = (c And 65280) \ 256
pic(2, i, j) = (c And &HFF0000) \ 65536
Next j
Next i

For i = 1 To n - 1
For j = 1 To m - 1
r = w(0) * pic(0, i - 1, j - 1) + w(1) * pic(0, i - 1, j) + w(2) * pic(0, i - 1, j + 1) + _
w(3) * pic(0, i, j - 1) + w(4) * pic(0, i, j) + w(5) * pic(0, i, j + 1) + _
w(6) * pic(0, i + 1, j - 1) + w(7) * pic(0, i + 1, j) + w(8) * pic(0, i + 1, j + 1)

g = w(0) * pic(1, i - 1, j - 1) + w(1) * pic(1, i - 1, j) + w(2) * pic(1, i - 1, j + 1) + _
w(3) * pic(1, i, j - 1) + w(4) * pic(1, i, j) + w(5) * pic(1, i, j + 1) + _
w(6) * pic(1, i + 1, j - 1) + w(7) * pic(1, i + 1, j) + w(8) * pic(1, i + 1, j + 1)

b = w(0) * pic(2, i - 1, j - 1) + w(1) * pic(2, i - 1, j) + w(2) * pic(2, i - 1, j + 1) + _
w(3) * pic(2, i, j - 1) + w(4) * pic(2, i, j) + w(5) * pic(2, i, j + 1) + _
w(6) * pic(2, i + 1, j - 1) + w(7) * pic(2, i + 1, j) + w(8) * pic(2, i + 1, j + 1)

If r > 255 Then r = 255
If r < 0 Then r = 0
If g > 255 Then g = 255
If g < 0 Then g = 0
If b > 255 Then b = 255
If b < 0 Then b = 0

win2.PSet (i, j), RGB(r, g, b)
Next j
Next i



End Sub

Private Sub 均衡直方图_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
win2.Cls
ReDim p(255), q(255), qq(255), hd(255) As Double

For i = 1 To win1.ScaleWidth
For j = 1 To win1.ScaleHeight
c = win1.Point(i, j)
r = c And &HFF
For k = 0 To 255
If k = r Then hd(k) = hd(k) + 1
Next k
Next j
Next i

For i = 0 To 255
p(i) = hd(i) / (win1.ScaleHeight * win1.ScaleWidth)
For j = 0 To i
q(i) = q(i) + p(j)
Next j
qq(i) = Int(q(i) * 100) / 100
Next i
For i = 0 To win1.ScaleWidth
For j = 0 To win1.ScaleHeight
c = win1.Point(i, j)
r = c And &HFF
k = qq(r) * 255
win2.PSet (i, j), RGB(k, k, k)
Next j
Next i
win1.ScaleMode = 1


End Sub

Private Sub 扩大_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
n = win1.ScaleWidth
m = win1.ScaleHeight
For i = 1 To n - 1
For j = 1 To m - 1
k = 255
c = win1.Point(i, j)
c = c And &HFF
f = win1.Point(i - 1, j)
f = f And &HFF
h = win1.Point(i, j - 1)
h = h And &HFF
l = win1.Point(i + 1, j)
l = l And &HFF
o = win1.Point(i, j + 1)
o = o And &HFF
If c = 0 Then
k = 0
End If
If c = 255 And (f = 0 Or h = 0 Or l = 0 Or o = 0) Then       '边界点扩大
k = 0
End If
If c = 255 And (f = 255 And h = 255 And l = 255 And o = 255) Then
k = 255
End If
win2.PSet (i, j), RGB(k, k, k)
Next j
Next i
End Sub

Private Sub 轮廓提取_Click()

win1.ScaleMode = 3
win2.ScaleMode = 3
n = win1.ScaleWidth
m = win1.ScaleHeight
For i = 0 To n - 1
  For j = 0 To m - 1
    c = win1.Point(i, j)
    c = c And &HFF
    f = win1.Point(i - 1, j)
    f = f And &HFF
    h = win1.Point(i, j - 1)
    h = h And &HFF
    l = win1.Point(i + 1, j)
    l = l And &HFF
    o = win1.Point(i, j + 1)
    o = o And &HFF
    If c = 255 Then k = 255
    If c = 0 And (f = 255 Or h = 255 Or l = 255 Or o = 255) Then
      k = 0
    Else: k = 255
    End If
    win2.PSet (i, j), RGB(k, k, k)
Next j
Next i


End Sub

Private Sub 收缩_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
n = win2.ScaleWidth
m = win2.ScaleHeight
For i = 1 To n - 1
For j = 1 To m - 1
k = 255
c = win2.Point(i, j)
f = win2.Point(i - 1, j)
h = win2.Point(i, j - 1)
l = win2.Point(i + 1, j)
o = win2.Point(i, j + 1)
If c = 255 Then
k = 255
End If
If c = 0 And (f = 255 Or h = 255 Or l = 255 Or o = 255) Then
k = 255
End If
If c = 0 And f = 0 And h = 0 And l = 0 And o = 0 Then
k = 0
End If
win1.PSet (i, j), RGB(k, k, k)
Next j
Next i

End Sub

Private Sub 图像细化_Click()
Dim a(9), b(9), c(9), d(9), e(9), g(9), n, p(1024, 1024), pic(1024, 1024, 1), s, i, j, k, t, w, h, r As Double
win1.ScaleMode = 3
win2.ScaleMode = 3
w = win1.ScaleWidth
h = win1.ScaleHeight
For j = 0 To h - 1
For i = 0 To w - 1
r = win1.Point(i, j)
pic(i, j, 0) = r And &HFF
If (pic(i, j, 0) = 0) Then
p(i, j) = 1
Else
p(i, j) = 0
End If
Next i
Next j
s = 1
Do While s <> 0
For i = 1 To w - 2
For j = 1 To h - 2
If (p(i, j) = 1) Then
a(1) = p(i, j + 1): a(2) = p(i - 1, j + 1): a(3) = p(i - 1, j)
a(4) = p(i - 1, j - 1): a(5) = p(i, j - 1): a(6) = p(i + 1, j - 1)
a(7) = p(i + 1, j): a(8) = p(i + 1, j + 1)
s = 0: For k = 0 To 8
If a(k) = 1 Then
c(k) = 1
Else
c(k) = 0
End If
Next k
c(9) = c(1)
f = 0
For k = 1 To 7 Step 2
f = f + (1 - c(k)) - (1 - c(k)) * (1 - c(k + 1)) * (1 - c(k + 2))
Next k
If f = 1 Then
s = 0: For k = 1 To 7 Step 2
s = s + Abs(a(k))

Next k
If s <= 3 Then
s = 0
For k = 1 To 8
s = s + Abs(a(k))
Next k
If s >= 2 Then
p(i, j) = -1
End If
End If
End If
End If
Next j
Next i
s = 0
For j = 0 To h - 1
For i = 0 To w - 1

If p(i, j) = -1 Then
p(i, j) = 0
s = 1
End If
Next i
Next j

For j = 0 To h - 1
For i = 0 To w - 1
If p(i, j) = 0 Then
win2.PSet (i, j), RGB(255, 255, 255)
Else
win2.PSet (i, j), RGB(0, 0, 0)
End If
Next i
Next j
Loop
End Sub



Private Sub 直方图_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
ReDim n1(255) As Double
直方图1.Show
直方图1.MSChart1.RowCount = 8
直方图1.MSChart1.ColumnCount = 32

直方图1.MSChart1.Row = 1: 直方图1.MSChart1.RowLabel = "32"
直方图1.MSChart1.Row = 2: 直方图1.MSChart1.RowLabel = "64"
直方图1.MSChart1.Row = 3: 直方图1.MSChart1.RowLabel = "96"
直方图1.MSChart1.Row = 4: 直方图1.MSChart1.RowLabel = "128"
直方图1.MSChart1.Row = 5: 直方图1.MSChart1.RowLabel = "160"
直方图1.MSChart1.Row = 6: 直方图1.MSChart1.RowLabel = "192"
直方图1.MSChart1.Row = 7: 直方图1.MSChart1.RowLabel = "224"
直方图1.MSChart1.Row = 8: 直方图1.MSChart1.RowLabel = "255"

For i = 0 To win1.ScaleWidth
For j = 0 To win1.ScaleHeight
c = win1.Point(i, j)
r = c And &HFF
For k = 0 To 255
If k = r Then n1(k) = n1(k) + 1
Next k
Next j
Next i

For i = 0 To 7
For j = 0 To 31
直方图1.MSChart1.Row = i + 1
直方图1.MSChart1.Column = j + 1
直方图1.MSChart1.Data = n1(i * 32 + j)
Next j
Next i
 


End Sub

Private Sub 质心提取_Click()

win1.ScaleMode = 3
win2.ScaleMode = 3
n = win2.ScaleWidth
m = win2.ScaleHeight
Dim m00, m10, m01 As Double
Dim xc, yc As Double

m00 = 0
m10 = 0
m01 = 0
For j = 1 To m
  For i = 1 To n
    c = win2.Point(i, j)
    c = c And &HFF
    m00 = c + m00
    m10 = m10 + i * c
    m01 = m01 + j * c
  Next i
Next j

xc = m10 / m00
yc = m01 / m00
For i = 0 To n
For j = 0 To m
r = win2.Point(i, j)
r = r And &HFF
win1.PSet (i, j), RGB(r, r, r)
Next j
Next i

win1.PSet (xc, yc), RGB(0, 0, 0)
Text1.Text = xc
Text2.Text = yc


 

End Sub

Private Sub 中值滤波_Click()
win1.ScaleMode = 3
win2.ScaleMode = 3
ReDim RZ(8) As Double
blocksize = 3       '领域大小为3*3

n = win1.ScaleWidth
m = win1.ScaleHeight

For i = 1 To n - 1
For j = 1 To m - 1
k = 0
For a1 = 0 To blocksize - 1
For a2 = 0 To blocksize - 1
c = win1.Point(i + a1, j + a2)   '求领域内各点的灰度值
RZ(k) = c And &HFF
k = k + 1
Next a2
Next a1

For e = 7 To 0 Step -1
 For s = 0 To b
 If RZ(s + 1) < RZ(s) Then
 d = RZ(s)                          '从小到大排序
 RZ(s) = RZ(s + 1)
 RZ(s + 1) = d
 End If
 Next s
 Next e
  
  If RZ(4) > 255 Then RZ(4) = 255
  If RZ(4) < 0 Then RZ(4) = 0

 win2.PSet (i, j), RGB(RZ(4), RZ(4), RZ(4))
Next j
Next i

End Sub

Private Sub 最大直径提取法二_Click()

Const pi = 3.1415926
Dim a, angle As Single
Dim r, p, kuan, gao As Integer
Dim h, q, s As Integer
r = 0
h = 0
p = 0
q = 0
kuan = 0
gao = 0
s = 0

win1.ScaleMode = 3
win2.ScaleMode = 3
n = win1.ScaleWidth
m = win1.ScaleHeight

For a = 0 To 90 Step 3
  For i = 0 To n
    For j = 0 To m
      c = win2.Point(i, j)
      c = c And &HFF
      If c = 0 Then
        If r > i Then r = i
        If h > j Then h = j
        If p < i Then p = i
        If q < j Then q = j
      End If
    Next j
  Next i
  kuan = p - r
  gao = q - h
  If s > kuan * gao Then
  s = kuan * gao
  lastk = kuan
  lastg = gao
  
win1.Cls
Call bmp_rotate(win1, win2, a) '调用图像旋转函数
Next a

If lastk > lastg Then zhijing = lastk
Else: zhijing = lastg

End Sub

Private Sub 最大直径提取法一_Click()

win1.ScaleMode = 3
win2.ScaleMode = 3
n = win1.ScaleWidth
m = win1.ScaleHeight
Dim s, k
s = 0
k = 0

For i = 0 To n
  For j = 0 To m
    
    c = win2.Point(i, j)
    c = c And &HFF
    If c = 0 Then
      For a = 0 To n
        For b = 0 To m
          l = win2.Point(a, b)
          l = l And &HFF
          If l = 0 Then
          d = Sqr((a - i) * (a - i) + (b - j) * (b - j))
            If d >= s Then s = d
          End If
        Next b
      Next a
      If s >= k Then
        k = s
      End If
    End If
          
Next j
Next i

End Sub
