VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "mscomm32.ocx"
Begin VB.Form Form1 
   Caption         =   " 激光通信客户端-Group4"
   ClientHeight    =   6615
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7695
   LinkTopic       =   "Form1"
   ScaleHeight     =   6615
   ScaleWidth      =   7695
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Caption         =   "信息"
      Height          =   3615
      Left            =   120
      TabIndex        =   17
      Top             =   2160
      Width           =   7455
      Begin VB.TextBox recievemsg 
         Height          =   855
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   4
         Top             =   840
         Width           =   6135
      End
      Begin VB.Frame Frame3 
         Caption         =   "文件发送"
         Height          =   1455
         Left            =   2760
         TabIndex        =   28
         Top             =   1920
         Width           =   4455
         Begin MSComDlg.CommonDialog CommonDialog1 
            Left            =   3840
            Top             =   120
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
         End
         Begin VB.CommandButton Command5 
            Caption         =   "发送文件"
            Height          =   255
            Left            =   2640
            TabIndex        =   31
            Top             =   960
            Width           =   1095
         End
         Begin VB.CommandButton Command4 
            Caption         =   "打开"
            Height          =   255
            Left            =   720
            TabIndex        =   30
            Top             =   960
            Width           =   1095
         End
         Begin VB.Label Label14 
            Caption         =   "文件地址:"
            Height          =   495
            Left            =   240
            TabIndex        =   29
            Top             =   360
            Width           =   3975
         End
      End
      Begin VB.TextBox recievebytes 
         Height          =   285
         Left            =   1080
         TabIndex        =   26
         Text            =   "0"
         Top             =   2400
         Width           =   855
      End
      Begin VB.TextBox sendbytes 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
         Height          =   285
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   24
         Text            =   "0"
         Top             =   1920
         Width           =   855
      End
      Begin VB.TextBox sendmsg 
         Height          =   285
         Left            =   1080
         TabIndex        =   23
         Top             =   360
         Width           =   6135
      End
      Begin VB.CommandButton Command3 
         Caption         =   "发送信息"
         Height          =   255
         Left            =   840
         TabIndex        =   22
         Top             =   2880
         Width           =   1095
      End
      Begin VB.Label la 
         Caption         =   "Bytes"
         Height          =   255
         Left            =   2040
         TabIndex        =   27
         Top             =   2400
         Width           =   495
      End
      Begin VB.Label Label13 
         Caption         =   "Bytes"
         Height          =   255
         Left            =   2040
         TabIndex        =   25
         Top             =   1920
         Width           =   495
      End
      Begin VB.Label Label12 
         Caption         =   "接收字节:"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   2400
         Width           =   855
      End
      Begin VB.Label Label11 
         Caption         =   "发送字节:"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   1920
         Width           =   975
      End
      Begin VB.Label Label8 
         Caption         =   "接收信息:"
         Height          =   375
         Left            =   240
         TabIndex        =   19
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label7 
         Caption         =   "发送信息:"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame 状态栏 
      Caption         =   "状态"
      Height          =   615
      Left            =   120
      TabIndex        =   15
      Top             =   5880
      Width           =   7455
      Begin VB.Label ztl 
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   7215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "设置"
      Height          =   1935
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7455
      Begin MSComDlg.CommonDialog Cd3 
         Left            =   240
         Top             =   1200
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSComDlg.CommonDialog Cd2 
         Left            =   6720
         Top             =   1200
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSCommLib.MSComm MSComm1 
         Left            =   6840
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         _Version        =   393216
         DTREnable       =   0   'False
         OutBufferSize   =   16384
      End
      Begin VB.ComboBox Combotzw 
         Height          =   315
         ItemData        =   "Form1.frx":0000
         Left            =   6360
         List            =   "Form1.frx":000A
         TabIndex        =   14
         Text            =   "1"
         Top             =   840
         Width           =   855
      End
      Begin VB.ComboBox Combosjw 
         Height          =   315
         ItemData        =   "Form1.frx":0014
         Left            =   4560
         List            =   "Form1.frx":0021
         TabIndex        =   13
         Text            =   "8"
         Top             =   840
         Width           =   855
      End
      Begin VB.ComboBox Combojyz 
         Height          =   315
         ItemData        =   "Form1.frx":002E
         Left            =   2760
         List            =   "Form1.frx":0041
         TabIndex        =   12
         Text            =   "n"
         Top             =   840
         Width           =   855
      End
      Begin VB.ComboBox Combobtl 
         Height          =   315
         ItemData        =   "Form1.frx":0054
         Left            =   960
         List            =   "Form1.frx":0064
         TabIndex        =   11
         Text            =   "9600"
         Top             =   840
         Width           =   855
      End
      Begin VB.ComboBox Combockh 
         Height          =   315
         ItemData        =   "Form1.frx":0080
         Left            =   960
         List            =   "Form1.frx":0090
         TabIndex        =   10
         Text            =   "1"
         Top             =   360
         Width           =   855
      End
      Begin VB.CommandButton Command2 
         Caption         =   "初始化"
         Height          =   255
         Left            =   5400
         TabIndex        =   8
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CommandButton Command1 
         Caption         =   "打开串口"
         Height          =   255
         Left            =   960
         TabIndex        =   7
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label Label16 
         Caption         =   "0"
         Height          =   255
         Left            =   4200
         TabIndex        =   34
         Top             =   1440
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label Label15 
         Caption         =   "0"
         Height          =   255
         Left            =   3360
         TabIndex        =   33
         Top             =   1440
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label label 
         Caption         =   "0"
         Height          =   255
         Left            =   2400
         TabIndex        =   32
         Top             =   1440
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label Label6 
         Caption         =   "串口通信状态：红色为待机，绿色为通信。"
         Height          =   255
         Left            =   2520
         TabIndex        =   9
         Top             =   360
         Width           =   3615
      End
      Begin VB.Shape Shape1 
         FillColor       =   &H000000FF&
         FillStyle       =   0  'Solid
         Height          =   255
         Left            =   2160
         Shape           =   3  'Circle
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label5 
         Caption         =   "停止位:"
         Height          =   255
         Left            =   5640
         TabIndex        =   6
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "数据位:"
         Height          =   255
         Left            =   3840
         TabIndex        =   5
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "检验值:"
         Height          =   255
         Left            =   2040
         TabIndex        =   3
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "波特率:"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "串口号:"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   615
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim a, l As Integer
    Dim BytReceived() As Byte
    Dim strData As String
    Dim bcdz As String
    Dim lenInput As Integer
    Dim bytSendByte() As Byte
    Dim strSendText As String
    Dim blnAutoSendFlag As Boolean
    Dim jswj As Byte
    Dim js As Byte
    Dim flag As Boolean
    Dim okm As Boolean
    Dim txjl As Boolean
    Dim jlm As Boolean
    Dim sum As Integer
    
Private Sub Command1_Click()
  If MSComm1.PortOpen = False Then
    MSComm1.CommPort = Combockh.Text
    MSComm1.PortOpen = True
    Command1.Caption = "关闭串口"
    MSComm1.Settings = Combobtl.Text & "," & Combojyz.Text & "," & Combosjw.Text & "," & Combotzw.Text
    ztl.Caption = "设置串口" & Combockh.Text & "成功！（波特率:" & Combobtl.Text & ",检验值:" & Combojyz.Text & ",数据位:" & Combosjw.Text & ",停止位:" & Combotzw.Text & "）"
    MSComm1.Output = "qqjl"
    Else
      MSComm1.PortOpen = False
      Command1.Caption = "打开串口"
      ztl.Caption = "串口关闭成功！"
      End If
End Sub

Private Sub Command2_Click()
If MSComm1.PortOpen = True Then
  MSComm1.PortOpen = False
  Command1.Caption = "打开串口"
  End If
  Combockh.Text = 1
  Combobtl.Text = 9600
  Combojyz.Text = "N"
  Combosjw.Text = 8
  Combotzw.Text = 1
  sendmsg.Text = ""
  recievemsg.Text = ""
  sendbytes.Text = 0
  recievebytes.Text = 0
  Label14.Caption = ""
  ztl.Caption = "初始化成功！"
  'flags还原
  txjl = False
  jlm = False
  flag = False
  okm = False
  jswj = 0
  sum = 0
  MSComm1.InBufferCount = 0   '清空接受缓冲区
  MSComm1.OutBufferCount = 0  '清空传输缓冲区
End Sub

Private Sub Command3_Click()
  Dim sj_Txt, lss As String
  Dim sj_txt_len As Integer
  Dim i, lo, po, jo, ko As Integer
  sj_Txt = sendmsg.Text
  lo = Len(sj_Txt) + 2
  If MSComm1.PortOpen = True Then
    If sj_Txt <> "" Then
        If okm = False Then
          fsxx
          End If
      While okm = True
        If lo > 8 Then
          ztl.Caption = "信息发送中..."
          Shape1.FillColor = vbGreen
          lss = Left(sj_Txt, 8)
          sj_Txt = Right(sj_Txt, Len(sj_Txt) - 8)
          lo = lo - 8
        Else
          Dim xxx, im As Integer
          xxx = 8 - lo
          lss = Left(sj_Txt, lo) & vbCrLf
          For im = 1 To xxx
            lss = lss & " "
            Next
          lo = 0
          End If
        MSComm1.Output = lss
        sj_txt_len = LenB(StrConv(lss, vbFromUnicode))
        sendbytes.Text = sendbytes.Text + sj_txt_len
        If lo = 0 Then
          Shape1.FillColor = vbRed
          okm = False
          
          MSComm1.Output = "cswc"
          ztl.Caption = "信息发送成功"
          End If
        Wend
      Else
        MsgBox "请输入内容！", vbOKOnly, "错误"
        End If
    Else
      MsgBox "请打开端口！", vbOKOnly, "错误"
    End If
   
End Sub

Private Sub Command4_Click()
CommonDialog1.Action = 1
Label14.Caption = CommonDialog1.FileName

End Sub

Private Sub Command5_Click()
  Dim ary() As Byte
  Dim iii As Integer
  Dim sss, ss1 As String
  Dim outbyte As Long
  If MSComm1.PortOpen = True Then
    sss = Label14.Caption
    If okm = False Then
     ' For iii = 1 To Len(sss)
      '  If sss(iii) = "?." Then
      iii = InStr(sss, ".")
          ss1 = Mid(sss, iii, Len(sss) - iii + 1)
    '      End If
    '    Next
      MSComm1.Output = "wj" & ss1
      End If

    If (okm = True) Then
      ztl.Caption = "文件发送中..."
      Shape1.FillColor = vbGreen
      Open Label14.Caption For Binary As #1
      label.Caption = LOF(1)
      outbyte = LOF(1)
      ReDim ary(LOF(1) - 1)
      Get #1, , ary()
      Form2.Visible = True
      MSComm1.InputMode = comInputModeBinary
      MSComm1.Output = ary
      label.Caption = 0
      'sendmsg.Text = ary
      sendbytes.Text = sendbytes.Text + outbyte
      MSComm1.InputMode = comInputModeText
      MSComm1.Output = "cswc"
      okm = False
      Shape1.FillColor = vbRed
      sum = 0
      Close #1
      Else
        sum = sum + 1
        If sum > 1 Then
          MsgBox "对方拒接接收文件", vbOKOnly, "拒绝"
          ztl.Caption = "发送文件失败"
          sum = 0
          End If
        End If
    Else
      MsgBox "请先打开端口！", vbOKOnly, "错误"
    End If
    
End Sub

Private Sub Form_Load()
  sum = 0
  If MSComm1.PortOpen = True Then
    MSComm1.PortOpen = False
    End If
  Shape1.FillColor = vbRed
  ztl.Caption = "欢迎！"

  jlm = False
  txjl = False
  flag = False
    MSComm1.InBufferCount = 0   '清空接受缓冲区
    MSComm1.OutBufferCount = 0  '清空传输缓冲区
    MSComm1.RThreshold = 1      '产生MSComm事件
    MSComm1.InBufferSize = 1024 * 16
    sendmsg = ""
    recievemsg = ""
  jswj = 0
  'ztl.Caption = MsgBox("we", vbYesNo, "ad")
End Sub


Private Sub MSComm1_OnComm() '接收数据
    Dim strBuff, strbuff2 As String
    Dim len2 As Integer
    Select Case MSComm1.CommEvent
        Case 2
            
            If (jswj <> 1) Then
            MSComm1.InputLen = 0
            strBuff = MSComm1.Input
            Dim ils As Integer
            ils = InStr(strBuff, " ")
            If ils = 0 Then
              ils = Len(strBuff) + 1
              End If
            strbuff2 = Left(strBuff, ils - 1)
            strBuff = strbuff2
            If strBuff = "jjl" Then
              ztl.Caption = "对方拒绝"
              MsgBox "对方拒绝", vbOKOnly, "拒绝"
              End If
            '判断是否建立连接
            If strBuff = "qqjl" Then
              Dim ty As Integer
              MSComm1.Output = "qqjl"
              ty = MsgBox("对方请求建立连接", vbYesNo, "连接请求")
              If ty = 6 Then
                jlm = True
                MSComm1.Output = "jllj"
                ztl.Caption = "等待握手"
                End If
              If ty = 7 Then
                jlm = False
                ztl.Caption = "您拒绝了对方的请求"
                MSComm1.Output = "jjl"
                End If

              End If
            If ((txjl <> True) And (jlm = True) And (strBuff <> "ok") And (strBuff <> "wj") And (strBuff <> "xx")) Then
              MSComm1.Output = "jllj"
              If strBuff = "jllj" Then
                txjl = True
                ztl.Caption = "握手成功，与对方建立通信！"
                'MSComm1.InBufferCount = 0   '清空接受缓冲区
                'MSComm1.OutBufferCount = 0  '清空传输缓冲区
                End If
              End If
            '判断是否发送文件
            If ((txjl = True) And (Mid(strBuff, 1, 2) = "wj")) Then
              Dim tg As Integer
              tg = MsgBox("对方发送文件，接收吗？", vbYesNo, "文件传输")
              If (tg = 6) Then
                jswj = 1
                'sum = sum + 1
                MSComm1.Output = "okw"
                flag = True
                ztl.Caption = "准备接收文件"
                Cd2.Action = 2
                bcdz = Cd2.FileName & Mid(strBuff, 3, Len(strBuff) - 2)
                Label16.Caption = bcdz
                Shape1.FillColor = vbGreen
                MSComm1.InputMode = comInputModeBinary
                ztl.Caption = "开始接收文件"
                Else
                  ztl.Caption = "您拒绝了对方发送文件的请求"
                  End If
              End If
            If ((txjl = True) And (strBuff = "xx")) Then
              MSComm1.Output = "okx"
              flag = True
              jswj = 2
              ztl.Caption = "准备接收信息"
              Shape1.FillColor = vbGreen
              End If
            If (txjl = True) And (strBuff = "okw") Then
              okm = True
              Command5_Click
              End If
            If (txjl = True) And (strBuff = "okx") Then
              okm = True
              Command3_Click
              End If
            Dim lslsls As String
            '接收信息
            If ((flag = True) And (strBuff <> "cswc") And (jswj = 2) And (txjl = True) And (strBuff <> "xx") And (strBuff <> "wj") And (strBuff <> "okw") And (strBuff <> "okx")) Then
              If Right(strBuff, 4) = "cswc" Then
                lslsls = Left(strBuff, Len(strBuff) - 4)
                Else
                lslsls = strBuff
                End If
              If recievemsg.Text = "" Then
                recievemsg.Text = lslsls
                Else
                  recievemsg.Text = recievemsg.Text & Trim(lslsls) '& vbCrLf
                End If
              len2 = LenB(StrConv(lslsls, vbFromUnicode))
              recievebytes.Text = recievebytes.Text + len2
              End If
            End If
            '接收文件
            If ((Right(strBuff, 4) = "cswc") And (txjl = True)) Then
              flag = False
              jswj = 0
              ztl.Caption = "数据接收成功"
              Shape1.FillColor = vbRed
              End If
            If ((flag = True) And (jswj = 1) And (txjl = True)) Then
              
              MSComm1.InputLen = 0
              Dim ary2() As Byte
              Open bcdz For Binary As #2
              ary2 = MSComm1.Input
              Label15.Caption = UBound(ary2)
              Form3.Visible = True
              Put #2, , ary2
              Label15.Caption = 0
              Close #2
              End If

    End Select
End Sub

Public Function jieshou() '接收数据处理为16进制
    Dim i As Integer
    Dim ls(2) As Byte
    For i = 0 To UBound(BytReceived)
        If Len(Hex(BytReceived(i))) = 1 Then
            strData = strData & BytReceived(i) & ","
        Else
            strData = strData & BytReceived(i) & ","
        End If
    Next
    recievemsg.Text = strData
End Function

Public Function fsxx()
  MSComm1.Output = "xx"
  End Function
