VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form3 
   Caption         =   "文件接收中..."
   ClientHeight    =   2415
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   5655
   LinkTopic       =   "Form3"
   ScaleHeight     =   2415
   ScaleWidth      =   5655
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   0
      Top             =   0
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   615
      Left            =   240
      TabIndex        =   1
      Top             =   1560
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   1085
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.TextBox Text1 
      Height          =   1095
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   5175
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Text1.Text = "保存地址:" & Form1.Label16.Caption & vbCrLf & "文件大小:" & Form1.Label15.Caption
ProgressBar1.Max = Form1.Label15.Caption
End Sub

Private Sub Timer1_Timer()
ProgressBar1.Value = ProgressBar1.Value + 1
If Form1.Label15.Caption = 0 Then
  ProgressBar1.Value = ProgressBar1.Max
  MsgBox "接收完成", vbOKOnly, "完成"
  Form3.Visible = False
  Timer1.Interval = 0
  End If
End Sub
