VERSION 5.00
Begin VB.Form frmdmain 
   Caption         =   "导航"
   ClientHeight    =   6450
   ClientLeft      =   1650
   ClientTop       =   2430
   ClientWidth     =   1755
   LinkTopic       =   "Form1"
   ScaleHeight     =   6450
   ScaleWidth      =   1755
   Begin VB.CommandButton mmcha 
      Caption         =   "更改密码"
      Height          =   495
      Left            =   240
      TabIndex        =   8
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton guanyu 
      Caption         =   "关于作者"
      Height          =   495
      Left            =   240
      TabIndex        =   7
      Top             =   5760
      Width           =   1215
   End
   Begin VB.CommandButton exitbotton 
      Caption         =   "退出"
      Height          =   495
      Left            =   240
      TabIndex        =   5
      Top             =   5040
      Width           =   1215
   End
   Begin VB.CommandButton returnbook 
      Caption         =   "还书"
      Height          =   495
      Left            =   240
      TabIndex        =   4
      Top             =   1440
      Width           =   1215
   End
   Begin VB.CommandButton borrowbook 
      Caption         =   "借书"
      Height          =   495
      Left            =   240
      TabIndex        =   3
      Top             =   720
      Width           =   1215
   End
   Begin VB.CommandButton selectbook 
      Caption         =   "图书查询"
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   2160
      Width           =   1215
   End
   Begin VB.CommandButton cardmanage 
      Caption         =   "借书证管理"
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   3600
      Width           =   1215
   End
   Begin VB.CommandButton insertbook 
      Caption         =   "新书入库"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   2880
      Width           =   1215
   End
   Begin VB.Label Label1 
      Height          =   420
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   1410
   End
End
Attribute VB_Name = "frmdmain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub borrowbook_Click()
frmborrow.Show
End Sub

Private Sub cardmanage_Click()
frmcardma.Show
End Sub

Private Sub exitbotton_Click()
Unload Me
End Sub

Private Sub Form_Load()
Label1.Caption = "您好，管理员" + Frmd1.managername + "。"
End Sub

Private Sub guanyu_Click()
frmpro.Show
End Sub

Private Sub insertbook_Click()
frminsert.Show
End Sub


Private Sub mmcha_Click()
mmch.Show
End Sub

Private Sub returnbook_Click()
frmreturn.Show
End Sub

Private Sub selectbook_Click()
frmselect.Show
End Sub
