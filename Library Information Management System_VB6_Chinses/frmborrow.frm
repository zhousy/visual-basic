VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmborrow 
   Caption         =   "借书"
   ClientHeight    =   6015
   ClientLeft      =   4155
   ClientTop       =   2190
   ClientWidth     =   8355
   LinkTopic       =   "Form1"
   ScaleHeight     =   6015
   ScaleWidth      =   8355
   Begin VB.CommandButton borrowbut 
      Caption         =   "借书"
      Height          =   375
      Left            =   5040
      TabIndex        =   9
      Top             =   5400
      Width           =   1815
   End
   Begin VB.TextBox bnoin 
      Height          =   375
      Left            =   1320
      TabIndex        =   6
      Top             =   4800
      Width           =   2055
   End
   Begin MSDataGridLib.DataGrid DataGrid2 
      Bindings        =   "frmborrow.frx":0000
      Height          =   495
      Left            =   1560
      TabIndex        =   4
      Top             =   3720
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   873
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc2 
      Height          =   375
      Left            =   2880
      Top             =   6360
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   1
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBString     =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   $"frmborrow.frx":0015
      Caption         =   "borrow_tj"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "frmborrow.frx":003D
      Height          =   2655
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   4683
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   375
      Left            =   6120
      Top             =   6360
      Visible         =   0   'False
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   1
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBString     =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   $"frmborrow.frx":0052
      Caption         =   "borrow"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton jscx 
      Caption         =   "确认"
      Height          =   375
      Left            =   4800
      TabIndex        =   2
      Top             =   240
      Width           =   1815
   End
   Begin VB.TextBox cnoin 
      Height          =   390
      Left            =   1320
      TabIndex        =   1
      Top             =   240
      Width           =   2175
   End
   Begin VB.Label Label4 
      Caption         =   "当前日期："
      Height          =   375
      Left            =   3840
      TabIndex        =   8
      Top             =   4800
      Width           =   4215
   End
   Begin VB.Label Label3 
      Caption         =   "当前管理员："
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Top             =   5400
      Width           =   3015
   End
   Begin VB.Label Label2 
      Caption         =   "图书序号："
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   4920
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "借书证号："
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   975
   End
End
Attribute VB_Name = "frmborrow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub borrowbut_Click()
Dim cn As New ADODB.Connection
cn.Open "driver={SQL Server};server=10.214.6.98;uid=user707;pwd=123456;database=db70"
Adodc1.RecordSource = "select stock from book where ((bno='" + bnoin.Text + "')and(stock>0))"
Adodc1.Refresh
If Adodc1.Recordset.RecordCount = 0 Then
  MsgBox "馆藏图书已全部借完！"
  Adodc1.RecordSource = "select * from borrow where bno='" + bnoin.Text + "' order by r_time desc"
  Adodc1.Refresh
Else
  MsgBox "借书成功！"
  cn.Execute "insert into borrow values('" + bnoin.Text + "','" + cnoin.Text + "','" + Format(Now()) + "',NULL,'" + Frmd1.managername + "')"
  cn.Execute "update book set stock=stock-1 where bno='" + bnoin.Text + "'"
  Adodc1.RecordSource = "select * From borrow where cno='" + cnoin.Text + "' order by r_time desc"
  Adodc1.Refresh
  Adodc2.RecordSource = "select 已借,已还,未还 from (select count(b_time),count(r_time),count(b_time)-count(r_time),cno from borrow group by cno) as brt(已借,已还,未还,cno) where cno='" + cnoin.Text + "'"
  Adodc2.Refresh
End If
End Sub

Private Sub Form_Load()
Label3.Caption = "当前管理员：" + Frmd1.managername
Label4.Caption = "当前日期：" + Format(Now, "yyyy年MM月dd日 hh:mm:ss dddd")
End Sub

Private Sub jscx_Click()
Adodc1.RecordSource = "select * From borrow where cno='" + cnoin.Text + "'"
Adodc1.Refresh
Adodc2.RecordSource = "select 已借,已还,未还 from (select count(b_time),count(r_time),count(b_time)-count(r_time),cno from borrow group by cno) as brt(已借,已还,未还,cno) where cno='" + cnoin.Text + "'"
Adodc2.Refresh
End Sub

