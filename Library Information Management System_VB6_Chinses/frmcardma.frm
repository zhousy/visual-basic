VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmcardma 
   Caption         =   "借书证管理"
   ClientHeight    =   3375
   ClientLeft      =   4455
   ClientTop       =   3630
   ClientWidth     =   6735
   LinkTopic       =   "Form1"
   ScaleHeight     =   3375
   ScaleWidth      =   6735
   Begin VB.Frame Frame1 
      Caption         =   "提示"
      Height          =   1095
      Left            =   5040
      TabIndex        =   12
      Top             =   480
      Width           =   1455
      Begin VB.Label Label5 
         Caption         =   "查找及删除功能仅限借书证号！"
         Height          =   615
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.CommandButton selecta 
      Caption         =   "查找"
      Height          =   375
      Left            =   3120
      TabIndex        =   11
      Top             =   720
      Width           =   1695
   End
   Begin VB.CommandButton deleting 
      Caption         =   "删除"
      Height          =   375
      Left            =   3120
      TabIndex        =   10
      Top             =   1320
      Width           =   1695
   End
   Begin VB.CommandButton adding 
      Caption         =   "添加"
      Height          =   375
      Left            =   3120
      TabIndex        =   9
      Top             =   120
      Width           =   1695
   End
   Begin VB.TextBox lbin 
      Height          =   390
      Left            =   1320
      TabIndex        =   8
      Top             =   1560
      Width           =   1455
   End
   Begin VB.TextBox dein 
      Height          =   390
      Left            =   1320
      TabIndex        =   7
      Top             =   1080
      Width           =   1455
   End
   Begin VB.TextBox namein 
      Height          =   375
      Left            =   1320
      TabIndex        =   6
      Top             =   600
      Width           =   1455
   End
   Begin VB.TextBox cnoin 
      Height          =   390
      Left            =   1320
      TabIndex        =   5
      Top             =   120
      Width           =   1455
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "frmcardma.frx":0000
      Height          =   1095
      Left            =   240
      TabIndex        =   0
      Top             =   2160
      Width           =   6255
      _ExtentX        =   11033
      _ExtentY        =   1931
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   -1  'True
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc cardman 
      Height          =   495
      Left            =   3360
      Top             =   3360
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   873
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   1
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBString     =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   $"frmcardma.frx":0016
      Caption         =   "card"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label4 
      Caption         =   "类    别："
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   1680
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "单    位："
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "姓    名："
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   720
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "借书证号："
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmcardma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub adding_Click()
If cnoin.Text = "" Then
  MsgBox "请至少输入借书证号！"
Else
 cardman.RecordSource = "select * from card where cno='" + cnoin.Text + "'"
 cardman.Refresh
 If cardman.Recordset.RecordCount = 0 Then
   Dim cn As New ADODB.Connection
   cn.Open "driver={SQL Server};server=10.214.6.98;uid=user707;pwd=123456;database=db70"
   cn.Execute "insert into card values('" + cnoin.Text + "','" + namein.Text + "','" + dein.Text + "','" + lbin.Text + " ')"
   cardman.RecordSource = "select * from card where cno='" + cnoin.Text + "'"
   cardman.Refresh
   MsgBox "增加借书证成功！"
 Else
   MsgBox "该借书证号已经存在！"
 End If
End If
End Sub

Private Sub deleting_Click()
If cnoin.Text = "" Then
  MsgBox "请输入借书证号！"
Else
  cardman.RecordSource = "select * from borrow where((cno='" + cnoin.Text + "')and(r_time is NULL)) order by bno"
  cardman.Refresh
  If cardman.Recordset.RecordCount = 0 Then
    Dim cn2 As New ADODB.Connection
    cn2.Open "driver={SQL Server};server=10.214.6.98;uid=user707;pwd=123456;database=db70"
    cn2.Execute "delete from card where cno='" + cnoin.Text + "'"
    cardman.RecordSource = "select * from card where cno='" + cnoin.Text + "'"
    cardman.Refresh
    MsgBox "删除借书证成功！"
    cnoin.Text = ""
    namein.Text = ""
    dein.Text = ""
    lbin.Text = ""
  Else
    MsgBox "该借书证尚有图书未还"
  End If
End If
End Sub

Private Sub selecta_Click()
If cnoin.Text = "" Then
  MsgBox "请输入借书证号！"
Else
    cardman.RecordSource = "select * from card where cno='" + cnoin.Text + "'"
    cardman.Refresh
End If
End Sub
