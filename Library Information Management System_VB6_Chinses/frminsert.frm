VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frminsert 
   Caption         =   "新书入库"
   ClientHeight    =   4620
   ClientLeft      =   4350
   ClientTop       =   2925
   ClientWidth     =   7860
   LinkTopic       =   "Form1"
   ScaleHeight     =   4620
   ScaleWidth      =   7860
   Begin VB.TextBox Text8 
      Height          =   390
      Left            =   1200
      TabIndex        =   23
      Top             =   3480
      Width           =   1575
   End
   Begin VB.CommandButton Command2 
      Caption         =   "批量导入"
      Height          =   375
      Left            =   4200
      TabIndex        =   20
      Top             =   3360
      Width           =   2535
   End
   Begin VB.TextBox Text10 
      Height          =   375
      Left            =   4680
      TabIndex        =   18
      Top             =   2280
      Width           =   2415
   End
   Begin VB.CommandButton Command1 
      Caption         =   "添加单本"
      Height          =   375
      Left            =   4440
      TabIndex        =   17
      Top             =   240
      Width           =   1695
   End
   Begin VB.TextBox Text9 
      Height          =   390
      Left            =   1200
      TabIndex        =   16
      Top             =   3960
      Width           =   1575
   End
   Begin VB.TextBox Text7 
      Height          =   375
      Left            =   1200
      TabIndex        =   15
      Top             =   3000
      Width           =   1575
   End
   Begin VB.TextBox Text6 
      Height          =   375
      Left            =   1200
      TabIndex        =   14
      Top             =   2520
      Width           =   1575
   End
   Begin VB.TextBox Text5 
      Height          =   390
      Left            =   1200
      TabIndex        =   13
      Top             =   2040
      Width           =   1575
   End
   Begin VB.TextBox Text4 
      Height          =   390
      Left            =   1200
      TabIndex        =   12
      Top             =   1560
      Width           =   1575
   End
   Begin VB.TextBox Text3 
      Height          =   375
      Left            =   1200
      TabIndex        =   11
      Top             =   1080
      Width           =   1575
   End
   Begin VB.TextBox Text2 
      Height          =   390
      Left            =   1200
      TabIndex        =   10
      Top             =   600
      Width           =   1575
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   1200
      TabIndex        =   9
      Top             =   120
      Width           =   1575
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "frminsert.frx":0000
      Height          =   975
      Left            =   3240
      TabIndex        =   0
      Top             =   840
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   1720
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc2 
      Height          =   495
      Left            =   6480
      Top             =   240
      Visible         =   0   'False
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   873
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   1
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBString     =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   $"frminsert.frx":0015
      Caption         =   "book"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label12 
      Caption         =   "有bug，需要把内容的格式换一下才能用不能用TAT……"
      Height          =   375
      Left            =   4200
      TabIndex        =   24
      Top             =   3960
      Width           =   2535
   End
   Begin VB.Label Label11 
      Caption         =   "馆藏总量："
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   3600
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "请把文件放在根目录下！"
      Height          =   255
      Left            =   4440
      TabIndex        =   21
      Top             =   2880
      Width           =   2055
   End
   Begin VB.Label Label10 
      Caption         =   "文件名："
      Height          =   255
      Left            =   3840
      TabIndex        =   19
      Top             =   2400
      Width           =   735
   End
   Begin VB.Label Label9 
      Caption         =   "现存总量："
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   4080
      Width           =   975
   End
   Begin VB.Label Label7 
      Caption         =   "价    格："
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   3120
      Width           =   975
   End
   Begin VB.Label Label6 
      Caption         =   "作    者："
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   2640
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "出版年份："
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "出 版 社："
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   1680
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "图书标题："
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "图书分类："
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   720
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "图书编号："
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frminsert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Dim cn As New ADODB.Connection
cn.Open "driver={SQL Server};server=10.214.6.98;uid=user707;pwd=123456;database=db70"
Dim sql As String
If Text1.Text = "" Then
  MsgBox "请至少输入图书编号！"
Else
  Adodc2.RecordSource = "select * from book where bno='" + Text1.Text + "'"
  Adodc2.Refresh
  If Adodc2.Recordset.RecordCount = 0 Then
    sqlstr = "insert into book values('" + Text1.Text + "','" + Text2.Text + "','" + Text3.Text + "','" + Text4.Text + "'," + Text5.Text + ",'" + Text6.Text + "'," + Text7.Text + "," + Text9.Text + "," + Text8.Text + ")"
    cn.Execute sqlstr
    Adodc2.RecordSource = "select * from book where bno='" + Text1.Text + "'"
    Adodc2.Refresh
    MsgBox "入库成功！"
  Else
    MsgBox "图书编号已经存在！"
    Text1.Text = ""
  End If
End If
End Sub

Private Sub Command2_Click()
  Dim TextLine
  Dim cn1 As New ADODB.Connection
  cn1.Open "driver={SQL Server};server=10.214.6.98;uid=user707;pwd=123456;database=db70"
  Open Text10.Text For Input As #1
  Do While Not EOF(1)
  Line Input #1, TextLine
  cn1.Execute "insert into book values" + TextLine
  Loop
  Close #1
  MsgBox "导入成功"
End Sub
