VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmselect 
   Caption         =   "图书查询"
   ClientHeight    =   6225
   ClientLeft      =   4245
   ClientTop       =   1845
   ClientWidth     =   9585
   LinkTopic       =   "Form1"
   ScaleHeight     =   6225
   ScaleWidth      =   9585
   Begin VB.CommandButton Command7 
      Caption         =   "尚有余量"
      Height          =   375
      Left            =   7320
      TabIndex        =   17
      Top             =   1080
      Width           =   1575
   End
   Begin VB.CommandButton Command6 
      Caption         =   "全部借出"
      Height          =   375
      Left            =   5160
      TabIndex        =   16
      Top             =   1080
      Width           =   1575
   End
   Begin VB.CommandButton Command5 
      Caption         =   "按作者查询"
      Height          =   375
      Left            =   3000
      TabIndex        =   15
      Top             =   1080
      Width           =   1575
   End
   Begin VB.CommandButton Command4 
      Caption         =   "按出版社查询"
      Height          =   375
      Left            =   7800
      TabIndex        =   14
      Top             =   600
      Width           =   1575
   End
   Begin VB.CommandButton Command3 
      Caption         =   "按书名查询"
      Height          =   375
      Left            =   3000
      TabIndex        =   13
      Top             =   600
      Width           =   1575
   End
   Begin VB.CommandButton Command2 
      Caption         =   "按类别查询"
      Height          =   375
      Left            =   7800
      TabIndex        =   12
      Top             =   120
      Width           =   1575
   End
   Begin VB.TextBox auin 
      Height          =   375
      Left            =   1200
      TabIndex        =   11
      Top             =   1080
      Width           =   1575
   End
   Begin VB.TextBox prin 
      Height          =   390
      Left            =   6000
      TabIndex        =   10
      Top             =   600
      Width           =   1575
   End
   Begin VB.TextBox tiin 
      Height          =   375
      Left            =   1200
      TabIndex        =   9
      Top             =   600
      Width           =   1575
   End
   Begin VB.TextBox cain 
      Height          =   375
      Left            =   6000
      TabIndex        =   8
      Top             =   120
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "按编号查询"
      Height          =   375
      Left            =   3000
      TabIndex        =   3
      Top             =   120
      Width           =   1575
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "frmselect.frx":0000
      Height          =   4335
      Left            =   120
      TabIndex        =   2
      Top             =   1680
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   7646
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   375
      Left            =   4200
      Top             =   6120
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   1
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBString     =   "Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;User ID=user707;Initial Catalog=db70;Data Source=10.214.6.98"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   $"frmselect.frx":0015
      Caption         =   "book"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox bnoin 
      Height          =   375
      Left            =   1200
      TabIndex        =   1
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label Label5 
      Caption         =   "作    者："
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "书    名："
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   720
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "图书类别："
      Height          =   255
      Left            =   4920
      TabIndex        =   5
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "出 版 社："
      Height          =   255
      Left            =   4920
      TabIndex        =   4
      Top             =   720
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "图书编号："
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmselect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Adodc1.RecordSource = "select * From book where bno='" + bnoin.Text + "'"
Adodc1.Refresh
End Sub

Private Sub Command2_Click()
Adodc1.RecordSource = "select * From book where category='" + cain.Text + "'"
Adodc1.Refresh
End Sub

Private Sub Command3_Click()
Adodc1.RecordSource = "select * From book where title='" + tiin.Text + "'"
Adodc1.Refresh
End Sub

Private Sub Command4_Click()
Adodc1.RecordSource = "select * From book where press='" + prin.Text + "'"
Adodc1.Refresh
End Sub

Private Sub Command5_Click()
Adodc1.RecordSource = "select * From book where author='" + auin.Text + "'"
Adodc1.Refresh
End Sub

Private Sub Command6_Click()
Adodc1.RecordSource = "select * From book where stock=0"
Adodc1.Refresh
End Sub

Private Sub Command7_Click()
Adodc1.RecordSource = "select * From book where stock>0"
Adodc1.Refresh
End Sub
